function Limpiar() {
    document.getElementById('txtValorAuto').value = "";
    document.getElementById('txtenganche').value = "";
    document.getElementById('txtFinanciar').value = "";
    document.getElementById('txtPago').value = "";
    document.getElementById('cmbPlan').value = 1;
}

function Calcular(){
    var valorAuto = document.getElementById('txtValorAuto').value;
    var opcion = document.getElementById('cmbPlan').value;
    if (isNaN(valorAuto) || valorAuto <= 0) {
        document.getElementById('txtValorAuto').value = '';
		return alert("Introduzca un valor valido");
	}
    var meses, intereses;
    var enganche = (valorAuto * 0.3).toFixed(2);
    switch (opcion) {
        case '1':
            meses=12;
            intereses=0.125;
            break;
        case '2':
            meses=18;
            intereses=0.172;
            break;
        case '3':
            meses=24;
            intereses=0.21;
            break;
        case '4':
            meses=36;
            intereses=0.26;
            break;
        case '5':
            meses=48;
            intereses=0.45;
            break;
        default:
            alert("Opción inválida");
            break;
    }
    document.getElementById('txtFinanciar').value = ((valorAuto - enganche) + ((valorAuto - enganche) * intereses)).toFixed(2);
    document.getElementById('txtenganche').value = enganche;
    document.getElementById('txtPago').value = (((valorAuto - enganche) + ((valorAuto - enganche) * intereses)) / meses).toFixed(2);
}
